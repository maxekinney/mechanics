﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public GameObject[] Players;
    //public GameObject[] PlayerUI;
    public int Turn;

    public Text PlayerUI;

    public Player[] playerScripts;
    // Start is called before the first frame update
    void Start()
    {
        Players = GameObject.FindGameObjectsWithTag("Player");
        //PlayerUI = gameObject.Find(
        for (int i = 0; i < Players.Length; i++)
        {
            //Debug.Log(i);
           // Debug.Log(Players.Length);
            playerScripts[i] = Players[i].GetComponent<Player>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        updateUI();
    }

    public void ChangeTurn()
    {
        for (int i = 0; i < 4; i++)
        {
            Players[i].SetActive(false);
            Debug.Log("Deactivate");
        }
        if (Turn == 3)
        {
            Turn = 0;
            StartCoroutine(TurnTimer());
        }
        else
        {
            Turn = Turn + 1;
            StartCoroutine(TurnTimer());
        }
        
        
    }

    public void GetResources()
    {
        for (int i = 0; i < playerScripts[Turn].Resources.Length; i++)
        {
            //Increase Resourses by number of mines
            playerScripts[Turn].Resources[i] = playerScripts[Turn].Resources[i] + playerScripts[Turn].Mines[i];
        }
    }

 
    public void updateUI()
    {
        PlayerUI.text = "Player " + (Turn + 1) + "'s Turn" +
            "\n" +
            "Resources: " + "\n" +
            "Resource 1: " + playerScripts[Turn].Resources[0] + "\n" +
            "Resource 2: " + playerScripts[Turn].Resources[1] + "\n" +
            "Resource 3: " + playerScripts[Turn].Resources[2] + "\n" +
            "Resource 4: " + playerScripts[Turn].Resources[3]
            ;

    }

    public IEnumerator TurnTimer()
    {
        yield return new WaitForSeconds(0.1f);
        Players[Turn].SetActive(true);
        playerScripts[Turn].GetResources();
    }
}
