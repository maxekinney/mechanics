﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextScript : MonoBehaviour
{
    //0-3
    public int PlayerTurn;
    public GameManager gameManager;
    // Start is called before the first frame update
    void Start()
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        PlayerTurn = gameManager.Turn;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
