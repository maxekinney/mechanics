﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MineScript : MonoBehaviour
{
    //Type of resource
    public int MineType;
    public GameManager gameManager;
    //Number of Mines added to player
    public int MineValue;

    public int FinishCardNum;
    public int[] MinePrice;
    public Text text;
    int playerMines;
    // Start is called before the first frame update
    void Start()
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        text.text = "" + MinePrice[0] + " " + MinePrice[1] +" " + MinePrice[2] +  " " + MinePrice[3];
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void AddMine()
    {
        
        var PlayerScript = gameManager.Players[gameManager.Turn].GetComponent<Player>();
        Debug.Log(PlayerScript);


        if (PlayerScript.Resources[0] >= MinePrice[0] && PlayerScript.Resources[1] >= MinePrice[1] && PlayerScript.Resources[2] >= MinePrice[2] && PlayerScript.Resources[3] >= MinePrice[3])
        {  //GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
            PlayerScript.Resources[0] = PlayerScript.Resources[0] - MinePrice[0];
            PlayerScript.Resources[1] = PlayerScript.Resources[1] - MinePrice[1];
            PlayerScript.Resources[2] = PlayerScript.Resources[2] - MinePrice[2];
            PlayerScript.Resources[3] = PlayerScript.Resources[3] - MinePrice[3];
            PlayerScript.Mines[MineType] = PlayerScript.Mines[MineType] + MineValue;

           // gameObject.SetActive(false);
        }
    }

    public void AddFinish()
    {
        var PlayerScript = gameManager.Players[gameManager.Turn].GetComponent<Player>();
        PlayerScript.FinishCards[FinishCardNum] = 1;
        PlayerScript.CheckFinishCard();

    }
}
