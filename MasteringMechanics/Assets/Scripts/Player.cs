﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{

    public int[] Resources;
    public int[] Mines;
    //public int FinishCards = 0;
    public bool win = false;

    public int[] FinishCards;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void Awake()
    {
        GetResources();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GetResources()
    {
        for (int i = 0; i < Resources.Length; i++)
        {
            //Increase Resourses by number of mines
            Resources[i] = Resources[i] + Mines[i];
        }
    }

    public void AddMine()
    {

    }

    public void CheckFinishCard()
    {
        if(FinishCards[0] == 1 && FinishCards[1] == 1 && FinishCards[2] == 1 && FinishCards[3] == 1)
        {
            win = true;
        }
    }

}
